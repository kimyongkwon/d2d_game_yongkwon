#pragma once

#include "stdafx.h"

class AnimatedSprite;
class System;

class GameResource
{
public:
	GameResource();
	~GameResource();

	HRESULT InitResource(System& system);
	void Release();

	void Update();
	void Draw();

private:
	AnimatedSprite*				m_pBackground;

	ID2D1HwndRenderTarget*			m_pRenderTarget;
};

