#pragma once

#include "stdafx.h"

class AnimationClip
{
private:
	PCWSTR			m_animationName;
	int				m_iMaxFrame;
	D2D1_RECT_F*	m_pRectArray;
public:
	AnimationClip(PCWSTR _wcAniName, int _iMaxFrame)
	{
		m_animationName = _wcAniName;
		m_iMaxFrame = _iMaxFrame;
		m_pRectArray = new D2D1_RECT_F[m_iMaxFrame];
	}

	~AnimationClip()
	{
		if (m_pRectArray)
		{
			delete[] m_pRectArray;
			m_pRectArray = nullptr;
		}
	}

	void AddFrame(int _iFrameNum, int _x, int _y, int _nx, int _ny)
	{
		assert(_iFrameNum < m_iMaxFrame);

		m_pRectArray[_iFrameNum].left = static_cast<FLOAT>(_x);
		m_pRectArray[_iFrameNum].top = static_cast<FLOAT>(_y);
		m_pRectArray[_iFrameNum].right = static_cast<FLOAT>(_x + _nx - 1);
		m_pRectArray[_iFrameNum].bottom = static_cast<FLOAT>(_y + _ny - 1);
	}

	int GetFrameMax(void) 
	{
		return m_iMaxFrame;
	}

	D2D1_RECT_F GetFrameRect(int _iFrame) 
	{
		return m_pRectArray[_iFrame];
	}
};