#pragma once

class CFrameRate
{
private:
	DWORD		m_dwTargetFPS;
	DWORD		m_dwNowFPS;
	DWORD		m_dwFrameCount;

	DWORD		m_dwFrameTick;
	float		m_dwPrevTick;

	DWORD		m_dwPrevSec;

public:
	CFrameRate(DWORD _dwFPS = 60);
	~CFrameRate();

	bool CheckSec(DWORD _currentTick);
	void Start(void);
	bool CalculateFPS(void);
	DWORD GetFPS(void) { return m_dwNowFPS; }
};

