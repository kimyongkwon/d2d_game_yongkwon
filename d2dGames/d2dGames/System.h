#pragma once
#include "stdafx.h"

class D2DDevice;
class GameResource;
class CFrameRate;

class System
{
public:
	System();
	System(const System&);
	~System();
	
private:
	HINSTANCE					m_hInst = nullptr;
	HWND						m_hWnd = nullptr;

public:
	bool Initialize(HINSTANCE hInstance, int nCmdShow);
	void Finalize();

	void Draw();
	void Update();

	void Run();
	
	HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
	HRESULT InitDevice(void);
	HRESULT InitResource(void);

	void ShowFrameRate(void);

	// 임시
public:
	HWND GetWindowHandler() { return m_hWnd; };
	
	// 평범하게 WndProc을 멤버함수르 쓰면 컴파일 에러
	//LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	D2DDevice*	m_pD2DDevice = nullptr;
	GameResource*	m_pGameResource = nullptr;
	CFrameRate*		m_pFrameRate = nullptr;

public:
	D2DDevice& GetD2DDevice() { return *m_pD2DDevice; };
};

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

