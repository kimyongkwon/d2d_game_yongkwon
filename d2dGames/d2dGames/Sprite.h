#pragma once

#include "stdafx.h"
#include "global.h"

class AnimationClip;

class Sprite
{
protected:
	ID2D1Bitmap*	m_pBitmap;
	D2D1_RECT_F		m_rectSource;
	D2D1_RECT_F		m_rectTarget;
	FLOAT		m_sizeX;
	FLOAT		m_sizeY;

protected:
	void Init(ID2D1Bitmap* _pBitmap)
	{
		m_pBitmap = _pBitmap;
		m_sizeX = m_pBitmap->GetSize().width;
		m_sizeY = m_pBitmap->GetSize().height;
		m_rectSource = m_rectTarget = D2D1::RectF(0.0f, 0.0f, m_sizeX - 1, m_sizeY - 1);
	}

	void Init(ID2D1Bitmap* _pBitmap, FLOAT _x, FLOAT _y, FLOAT _nx, FLOAT _ny)
	{
		m_pBitmap = _pBitmap;
		m_sizeX = _nx;
		m_sizeY = _ny;
		m_rectSource = m_rectTarget = D2D1::RectF(_x, _y, _x + _nx - 1, _y + _ny - 1);
	}

public:
	Sprite(ID2D1Bitmap* _pBitmap);

	Sprite(ID2D1Bitmap* _pBitmap, FLOAT _x, FLOAT _y, FLOAT _nx, FLOAT _ny);

	Sprite(PCWSTR _wcFileName);

	~Sprite();

	void SetPosition(FLOAT _x, FLOAT _y)
	{
		m_rectTarget = D2D1::RectF(_x, _y, _x + m_sizeX - 1, _y + m_sizeY - 1);
	}

	void Draw(ID2D1HwndRenderTarget& renderTarget);

	void Draw(ID2D1HwndRenderTarget& renderTarget, FLOAT _tx, FLOAT _ty, FLOAT _alpha);

	void Draw(ID2D1HwndRenderTarget& renderTarget, FLOAT _tx, FLOAT _ty);
};

//class CLAnimationData
//{
//private:
//	ID2D1Bitmap*		m_pBitmap;
//	int					m_iMaxClip;
//	AnimationClip*	m_pActiveClip;
//	AnimationClip**	m_pClipBuffer;
//public:
//private:
//
//public:
//	CLAnimationData(ID2D1Bitmap* _pBitmap, int _iMaxClip) : m_iMaxClip(_iMaxClip)
//	{
//		m_pBitmap = _pBitmap;
//		m_pActiveClip = nullptr;
//		m_pClipBuffer = new AnimationClip*[m_iMaxClip];
//	}
//
//	~CLAnimationData()
//	{
//		for (int i = m_iMaxClip - 1; i >= 0; i--)
//		{
//			if (m_pClipBuffer[i]) 
//			{
//				delete m_pClipBuffer[i]; 
//				m_pClipBuffer[i] = nullptr;
//			}
//		}
//
//		//SafeRelease(&m_pBitmap);
//	}
//
//	ID2D1Bitmap* GetBitmap(void) {
//		return m_pBitmap;
//	}
//
//	int GetClipMax(void) {
//		return m_iMaxClip;
//	}
//
//	bool AddClip(int _numClip, AnimationClip* _clip)
//	{
//		if (_numClip >= m_iMaxClip) 
//			return false;
//		m_pClipBuffer[_numClip] = _clip;
//
//		return true;
//	}
//
//	AnimationClip* GetClip(int _numClip)
//	{
//		if (_numClip >= m_iMaxClip) 
//			return nullptr;
//		return m_pClipBuffer[_numClip];
//	}
//};

class AnimatedSprite : public Sprite
{
private:
	AnimationClip*			m_pClipBuffer;
	int						m_iNowFrame;

public:

	AnimatedSprite(ID2D1Bitmap* _pBitmap) : Sprite(_pBitmap)
	{
		m_iNowFrame = 0;
	}

	~AnimatedSprite()
	{
		if (m_pClipBuffer)
		{
			delete m_pClipBuffer;
			m_pClipBuffer = nullptr;
		}
	}

	void AddClip(AnimationClip* _clip)
	{
		m_pClipBuffer = _clip;
	}

	void UpdateFrame(void);
};