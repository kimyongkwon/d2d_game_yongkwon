
#include "Sprite.h"
#include "System.h"
#include "D2DDevice.h"
#include "Animation.h"

Sprite::Sprite(ID2D1Bitmap* _pBitmap)
{
	Init(_pBitmap);
}

Sprite::Sprite(ID2D1Bitmap* _pBitmap, FLOAT _x, FLOAT _y, FLOAT _nx, FLOAT _ny)
{
	Init(_pBitmap, _x, _y, _nx, _ny);
}

Sprite::Sprite(PCWSTR _wcFileName)
{
	/*ID2D1Bitmap * pBitmap;
	if (FAILED(LoadBitmapFromFile(_wcFileName, &pBitmap)))
		throw std::runtime_error("Failed Load file");
	Init(pBitmap);*/
}

Sprite::~Sprite()
{
	SafeRelease(&m_pBitmap);
}

void Sprite::Draw(ID2D1HwndRenderTarget& renderTarget)
{
	renderTarget.DrawBitmap(
		m_pBitmap,
		m_rectTarget,
		1.0f, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
		m_rectSource);
}

void Sprite::Draw(ID2D1HwndRenderTarget& renderTarget, FLOAT _tx, FLOAT _ty, FLOAT _alpha)
{
	renderTarget.DrawBitmap(
		m_pBitmap,
		D2D1::RectF(_tx, _ty, _tx + m_sizeX - 1, _ty + m_sizeY - 1),
		_alpha, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
		m_rectSource);
}

void Sprite::Draw(ID2D1HwndRenderTarget& renderTarget, FLOAT _tx, FLOAT _ty)
{
	renderTarget.DrawBitmap(
		m_pBitmap,
		D2D1::RectF(_tx, _ty, _tx + m_sizeX - 1, _ty + m_sizeY - 1),
		1.0, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
		m_rectSource);
}

//////////////////////////////////////////////////////////////

void AnimatedSprite::UpdateFrame()
{
		m_iNowFrame = (m_iNowFrame + 1) % m_pClipBuffer->GetFrameMax();
		m_rectSource = m_pClipBuffer->GetFrameRect(m_iNowFrame);
}
