
#include "global.h"
#include "FrameRate.h"
#include "System.h"
#include "D2DDevice.h"

template<class T>
inline void SafeRelease(T** ppInterfaceToRelease);

namespace BitmapHelper
{
	HRESULT LoadBitmapFromFile(PCWSTR _wcFileName, ID2D1Bitmap** _ppBitmap, IWICImagingFactory& factory, ID2D1HwndRenderTarget& renderTarget)
	{
		HRESULT hr = S_OK;
		IWICBitmapDecoder*		pDecoder = nullptr;

		// 1. load image
		hr = factory.CreateDecoderFromFilename(
			_wcFileName,	// image file name
			NULL,			// do not prefer a particular vendor
			GENERIC_READ,	// disired read access to the file
			WICDecodeMetadataCacheOnDemand,	// cache metadata when needed
			&pDecoder);

		if (FAILED(hr)) return hr;

		// 2. 0번 프레임을 얻기
		IWICBitmapFrameDecode*	pFrame = nullptr;
		hr = pDecoder->GetFrame(0, &pFrame);
		if (FAILED(hr)) return hr;

		// 3. 컨버터 생성
		IWICFormatConverter*	pConverter = nullptr;
		hr = factory.CreateFormatConverter(&pConverter);
		if (FAILED(hr)) return hr;

		hr = pConverter->Initialize(pFrame,
			GUID_WICPixelFormat32bppPBGRA,
			WICBitmapDitherTypeNone,
			NULL,
			0.0f,
			WICBitmapPaletteTypeCustom);
		if (FAILED(hr)) return hr;

		// Create a Direct2D bitmap from the WIC bitmap,
		hr = renderTarget.CreateBitmapFromWicBitmap(
			pConverter,
			NULL,
			_ppBitmap);
		if (FAILED(hr)) return hr;

		if (pConverter) { pConverter->Release(); pConverter = nullptr; }
		if (pFrame) { pFrame->Release(); pFrame = nullptr; }
		if (pDecoder) { pDecoder->Release(); pDecoder = nullptr; }

		return hr;
	}
}