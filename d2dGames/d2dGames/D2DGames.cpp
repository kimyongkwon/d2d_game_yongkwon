
#include "stdafx.h"
#include "global.h"
#include "Sprite.h"
#include "FrameRate.h"
#include "System.h"

#define KEY_LEFT 0x41		// a
#define KEY_RIGHT 0x44		// d

void KeyInput()
{
	if (GetAsyncKeyState(KEY_LEFT) & 0x8000)
	{
		PostQuitMessage(0);
	}
	if (GetAsyncKeyState(KEY_RIGHT) & 0x8000)
	{
		PostQuitMessage(0);
	}
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
		// 윈도우가 닫히는지 확인합니다
	case WM_CLOSE:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		KeyInput();
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	System* GameSystem = new System;

	if (GameSystem->Initialize(hInstance, nCmdShow))
	{
		GameSystem->Run();
	}

	GameSystem->Finalize();
	delete GameSystem;
	GameSystem = nullptr;

	return 0;
}