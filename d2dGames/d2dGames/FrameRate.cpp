
#include "global.h"
#include "FrameRate.h"

CFrameRate::CFrameRate(DWORD _dwFPS /*= 60*/)
{
	m_dwTargetFPS = _dwFPS;
	m_dwFrameTick = 1000 / m_dwTargetFPS;
	m_dwPrevTick = 0;
	m_dwNowFPS = 0;
	m_dwFrameCount = 0;

	m_dwPrevSec = 0;
}

CFrameRate::~CFrameRate()
{
}

bool CFrameRate::CheckSec(DWORD _currentTick)
{
	DWORD nowTick = _currentTick - m_dwPrevSec;
	if (nowTick >= 1000)
	{
		m_dwPrevSec += 1000;
		m_dwNowFPS = m_dwFrameCount;
		m_dwFrameCount = 0;
		return true;
	}
	return false;
}

void CFrameRate::Start(void)
{
	m_dwPrevSec = m_dwPrevTick = timeGetTime();
	m_dwFrameCount = m_dwNowFPS = 0;
}

bool CFrameRate::CalculateFPS(void)
{
	DWORD currentTick = timeGetTime();
	CheckSec(currentTick);
	DWORD elapsed = currentTick - m_dwPrevTick;
	if (elapsed >= m_dwFrameTick)
	{
		m_dwFrameCount++;
		m_dwPrevTick = currentTick;
		return true;
	}

	return false;
}
