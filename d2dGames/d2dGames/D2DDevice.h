#pragma once

#include "stdafx.h"

class System;

class D2DDevice
{
public:
	D2DDevice();
	~D2DDevice();

private:
	ID2D1Factory*				m_pD2DFactory = nullptr;
	ID2D1HwndRenderTarget*		m_pRenderTarget = nullptr;
	ID2D1SolidColorBrush*		m_pBlackBrush = nullptr;

	IDWriteFactory*				m_pDWriteFactory = nullptr;
	IDWriteTextFormat*			m_pDWTextFormat = nullptr;
	
	IWICImagingFactory*			m_pWICFactory = nullptr;

public:
	HRESULT Initialize(System* system);
	void CleanupDevice(void);

public:
	IWICImagingFactory& GetIDWriteFactory() { return *m_pWICFactory; };
	ID2D1HwndRenderTarget& GetRenderTarget() { return *m_pRenderTarget; };
};

