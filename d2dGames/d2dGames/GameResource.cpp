#include "GameResource.h"
#include "Sprite.h"
#include "System.h"
#include "D2DDevice.h"
#include "Animation.h"

GameResource::GameResource()
{
}


GameResource::~GameResource()
{
}

HRESULT GameResource::InitResource(System& system)
{
	D2DDevice& d2dDevice = system.GetD2DDevice();
	IWICImagingFactory& WICFactory = d2dDevice.GetIDWriteFactory();
	m_pRenderTarget = &(d2dDevice.GetRenderTarget());

	if (!m_pRenderTarget)
		return E_FAIL;

	HRESULT hr = S_OK;

	ID2D1Bitmap* pBackGround = nullptr;
	hr = BitmapHelper::LoadBitmapFromFile(L"midnight.png", &pBackGround, WICFactory, *m_pRenderTarget);
	if (FAILED(hr)) return hr;

	AnimationClip* clip = new AnimationClip(L"Idle", 4);
	clip->AddFrame(0, 0, 0, 785, 321);
	clip->AddFrame(1, 789, 0, 785, 321);
	clip->AddFrame(2, 0, 325, 785, 321);
	clip->AddFrame(3, 789, 325, 785, 321);

	m_pBackground = new AnimatedSprite(pBackGround);
	m_pBackground->AddClip(clip);

	return hr;
}

void GameResource::Release()
{
	if (m_pBackground)
		delete m_pBackground;
}

void GameResource::Update()
{
	m_pRenderTarget->BeginDraw();
	m_pRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::CadetBlue));

	m_pBackground->UpdateFrame();
}

void GameResource::Draw()
{
	m_pBackground->Draw(*m_pRenderTarget, 0.0f, 0.0f);

	m_pRenderTarget->EndDraw();
}
