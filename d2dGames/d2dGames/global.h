#pragma once

#ifndef    __GLOBAL_H         
#define    __GLOBAL_H          

#include "stdafx.h"
#include "Sprite.h"

namespace BitmapHelper
{
	HRESULT LoadBitmapFromFile(PCWSTR _wcFileName, ID2D1Bitmap** _ppBitmap, IWICImagingFactory& factory, ID2D1HwndRenderTarget& renderTarget);
}

template<class T>
inline void SafeRelease(T** ppInterfaceToRelease)
{
	if (*ppInterfaceToRelease != NULL)
	{
		(*ppInterfaceToRelease)->Release();
		(*ppInterfaceToRelease) = NULL;
	}
}

#endif