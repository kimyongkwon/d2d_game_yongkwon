#include "D2DDevice.h"

#include "System.h"

D2DDevice::D2DDevice()
{
}

D2DDevice::~D2DDevice()
{
}

HRESULT D2DDevice::Initialize(System* system)
{
	HRESULT hr = S_OK;

	RECT	rc;
	GetClientRect(system->GetWindowHandler(), &rc);

	// Create a Direct2D factory 
	hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pD2DFactory);
	if (FAILED(hr)) return hr;

	// Craete a Direct2D render target
	hr = m_pD2DFactory->CreateHwndRenderTarget(
		D2D1::RenderTargetProperties(),
		D2D1::HwndRenderTargetProperties(system->GetWindowHandler(),
			D2D1::SizeU(rc.right - rc.left, rc.bottom - rc.top)),
		&m_pRenderTarget);
	if (FAILED(hr)) return hr;

	hr = m_pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::AliceBlue), &m_pBlackBrush);
	if (FAILED(hr)) return hr;

	hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED,
		__uuidof(IDWriteFactory),
		reinterpret_cast<IUnknown **>(&m_pDWriteFactory));
	if (FAILED(hr)) return hr;

	static const WCHAR fontName[] = L"Gabriola";
	const FLOAT fontSize = 50.0f;

	hr = m_pDWriteFactory->CreateTextFormat(
		fontName, NULL,
		DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		fontSize, L"en-us", &m_pDWTextFormat);

	CoInitialize(NULL);

	hr = CoCreateInstance(CLSID_WICImagingFactory, NULL,
		CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&m_pWICFactory));

	return hr;
}

void D2DDevice::CleanupDevice(void)
{
	if (m_pRenderTarget) m_pRenderTarget->Release();
	if (m_pD2DFactory) m_pD2DFactory->Release();
	if (m_pBlackBrush) m_pBlackBrush->Release();
	if (m_pDWriteFactory) m_pDWriteFactory->Release();
	if (m_pDWTextFormat) m_pDWTextFormat->Release();
	if (m_pWICFactory) CoUninitialize();
}