#include "System.h"
#include "D2DDevice.h"
#include "global.h"
#include "GameResource.h"
#include "Sprite.h"
#include "FrameRate.h"

System::System()
{
}

System::System(const System& other)
{
}

System::~System()
{
}

bool System::Initialize(HINSTANCE hInstance, int nCmdShow)
{
	if (FAILED(InitWindow(hInstance, nCmdShow)))
		return false;

	if (FAILED(InitDevice()))
		return false;

	if (FAILED(InitResource()))
		return false;

	m_pFrameRate = new CFrameRate(10);
	m_pFrameRate->Start();

	return true;
}

void System::Finalize()
{
	if (m_pGameResource)
	{
		m_pGameResource->Release();
		delete m_pGameResource;
		m_pGameResource = nullptr;
	}

	if (m_pFrameRate)
	{
		delete m_pFrameRate;
		m_pFrameRate = nullptr;
	}

	if (m_pD2DDevice)
	{
		m_pD2DDevice->CleanupDevice();
		delete m_pD2DDevice;
		m_pD2DDevice = nullptr;
	}

	if (m_pFrameRate)
	{
		delete m_pFrameRate;
		m_pFrameRate = nullptr;
	}

	//DestroyWindow(m_hWnd);
	//m_hWnd = nullptr;
}

void System::Draw()
{
	if (m_pGameResource)
		m_pGameResource->Draw();
}

void System::Update()
{
	if (m_pGameResource)
		m_pGameResource->Update();

	ShowFrameRate();
}

void System::Run()
{
	// Main message loop
	MSG msg = { 0 };
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if (m_pFrameRate->CalculateFPS())
			{
				Update();
			}
			Draw();
		}
	}
}



HRESULT System::InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	// Register class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = L"D2DTutWindowClass";
	wcex.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);
	if (!RegisterClassEx(&wcex))
		return E_FAIL;

	// Create window
	m_hInst = hInstance;
	RECT rc = { 0, 0, 800, 600 };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

	m_hWnd = CreateWindow(L"D2DTutWindowClass", L"D2DGames : simple app",
		WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInstance,
		nullptr);
	if (!m_hWnd) return E_FAIL;

	ShowWindow(m_hWnd, nCmdShow);

	return S_OK;
}

HRESULT System::InitDevice(void)
{
	m_pD2DDevice = new D2DDevice;

	if (FAILED(m_pD2DDevice->Initialize(this)))
	{
		m_pD2DDevice->CleanupDevice();
		delete m_pD2DDevice;
		return E_FAIL;
	}

	return S_OK;
}

HRESULT System::InitResource(void)
{
	m_pGameResource = new GameResource;

	if (FAILED(m_pGameResource->InitResource(*this)))
	{
		m_pGameResource->Release();
		delete m_pGameResource;
		return E_FAIL;
	}

	return S_OK;
}

void System::ShowFrameRate(void)
{
	WCHAR buf[32] = { 0, };
	wsprintf(buf, L"D2DProject (FPS : %d)", m_pFrameRate->GetFPS());
	SetWindowTextW(m_hWnd, buf);
}
